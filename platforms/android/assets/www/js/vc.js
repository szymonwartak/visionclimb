
$(function() {
    Parse.$ = jQuery;
    Parse.initialize("9Ax19KBDzrB4220HdTUUf0ARGePJBmgumcTyWsNH", "OWsMgH2WUfsznGFItqpUEPIFC7G5dMizCaQy5Dws");

    var Area = Parse.Object.extend("Area", {
        defaults: {
            name: "empty area",
            latitude: 0,
            longitude: 0
        }
    })
    var AreaList = Parse.Collection.extend({
        model: Area
    })
    var Route = Parse.Object.extend("Route", {
        defaults: {
            content: "empty route",
            latitude: 0,
            longitude: 0
        }
    })
    var RouteList = Parse.Collection.extend({
        model: Route
    })

    var AreaView = Parse.View.extend({
        tagName: "li",
        template: _.template($('#area-template').html()),
        events: {
            "click .area-item": "open"
        },
        render: function() {
            $(this.el).html(this.template(this.model.toJSON()));
            return this;
        },
        open: function() {
            new AreaRoutesView({areaId: this.model.id})
        }
    })

    var AllAreasView = Parse.View.extend({
        el: "#areas-list",
        initialize: function() {
            var self = this;
            _.bindAll(this, 'addOne', 'addAll');
        },
        addOne: function(area) {
            var view = new AreaView({model: area});
            this.$el.append(view.render().el);
        },
        addAll: function(collection, filter) {
            this.$el.html("")
            areas.each(this.addOne);
        }
    })
    var RouteView = Parse.View.extend({
        tagName: "li",
        template: _.template($('#route-template').html()),
        events: {
            "click label.todo-content": "edit",
            "keypress .edit": "updateOnEnter",
            "blur .edit": "close"
        },
        initialize: function() {
            _.bindAll(this, 'render', 'close', 'edit', 'updateOnEnter');
            this.model.bind('change', this.render);
        },
        render: function() {
            $(this.el).html(this.template(this.model.toJSON()));
            this.input = this.$('.edit');
            return this;
        },
        edit: function() {
            $(this.el).addClass("editing");
            this.input.focus();
        },
        close: function() {
            this.model.save({
                content: this.input.val()
            });
            $(this.el).removeClass("editing");
        },
        updateOnEnter: function(e) {
            if (e.keyCode == 13) this.close();
        }
    });

    var Map = Parse.Object.extend("Map", {
        defaults: {
            globalZoom:7,
            areaZoom:14
        }
    })
    var MapView = Parse.View.extend({
        initialize: function() {
            this.resetMap()
        },
        resetMap: function() {
            $('#map_canvas').gmap({
            zoom: this.model.globalZoom,
            center: new google.maps.LatLng(43.465895, -80.559296),
            'mapTypeControl' : false,
            'navigationControl' : false,
            'streetViewControl' : false,
            panControl: false,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL,
                position: google.maps.ControlPosition.TOP_LEFT
            },
            'callback': function() {
            	var self = this;
            	self.getCurrentPosition(function(position, status) {
            		if ( status === 'OK' ) {
            			var clientPosition = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            			this.model.clientPosition = clientPosition
            			self.addMarker({'position': clientPosition, 'bounds': true});
            			self.addShape('Circle', {
            				'strokeWeight': 0,
            				'fillColor': "#008595",
            				'fillOpacity': 0.25,
            				'center': clientPosition,
            				'radius': 15,
            				'clickable': false
            			});
            		}
            	});
            }});
            $(".ui-content").height($(window).height());
        }
    })
    var AreaRoutesView = Parse.View.extend({
        el: "#routes",
        events: {
            "keypress #add-route": "createOnEnter"
        },
        initialize: function(options) {
            this.input = this.$("#add-route")
            var self = this;
            _.bindAll(this, 'addOne', 'addAll', 'createOnEnter');
            this.areaId = options["areaId"]
            this.routes = new RouteList;
            this.routes.query = new Parse.Query(Route);
            this.routes.query.equalTo("areaId", this.areaId);
            this.routes.bind('add', this.addOne);
            this.routes.bind('all', this.addAll);
            this.routes.fetch();
        },
        addOne: function(route) {
            var view = new RouteView({model: route});
            var element = view.render().el
            this.$("#routes-list").append(element);
            element
        },
        addAll: function(collection, filter) {
            this.$("#routes-list").html("")
            this.routes.each(this.addOne);
        },
        createOnEnter: function(e) {
            if (e.keyCode != 13) return;
            this.routes.create({
                content: this.input.val(),
                areaId: this.areaId
            })
            this.input.val("")
        }

    })

    var map = new MapView({model: (new Map).toJSON()})

    var areasView = new AllAreasView;
    var areas = new AreaList;
    areas.query = new Parse.Query(Area);
    areas.bind('add', areasView.addOne);
    areas.bind('all', areasView.addAll);
    areas.fetch();

});